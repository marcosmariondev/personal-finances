CONTAINER_NAME=finance-app

docker-up:
	docker-compose up -d

docker-down:
	docker-compose down

docker-clear: docker-up
	docker exec $(CONTAINER_NAME) sh -c "php artisan optimize:clear"

docker-compose-install:
	make docker-build
	make docker-up
	make docker-compose-install
	make-docker-clear

docker-build:
	docker-compose build

docker-format: docker-up
	docker exec -it $(CONTAINER_NAME) composer format

docker-bash: docker-up
	docker exec -it $(CONTAINER_NAME) sh

docker-test: docker-up docker-clear
	docker exec -it $(CONTAINER_NAME) php artisan test

phpstan: docker-up
	docker exec -it $(CONTAINER_NAME) composer code-analyse

docker-test-coverage: docker-up docker-clear
	docker exec -e XDEBUG_MODE=coverage $(CONTAINER_NAME) vendor/bin/phpunit -d memory_limit=-1 --testdox --coverage-html .reports/coverage

