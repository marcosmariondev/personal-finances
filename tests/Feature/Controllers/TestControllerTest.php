<?php

declare(strict_types=1);


// use Illuminate\Foundation\Testing\RefreshDatabase;

namespace Controllers;

use Illuminate\Http\Response;
use Tests\TestCase;

class TestControllerTest extends TestCase
{
    public function testShouldReturnOk(): void
    {
        $response = $this->get('api/show/1');

        $response
            ->assertStatus(Response::HTTP_OK);
    }
}
