module.exports = {
    extends: ['@commitlint/config-conventional'],
    rules: {
        'scope-case': [2, 'always', 'kebab-case'], // Define que o escopo deve ser em kebab-case
        'subject-case': [2, 'always', 'sentence-case'], // Define que o subject deve estar em sentence-case
    }
};
