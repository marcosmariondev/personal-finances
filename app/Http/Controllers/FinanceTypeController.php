<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\StoreFinanceTypeRequest;
use App\Http\Requests\UpdateFinanceTypeRequest;
use App\Models\FinanceType;

class FinanceTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {





        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        if($i = 1) {
            return 1;
        }
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreFinanceTypeRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(FinanceType $financeType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(FinanceType $financeType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateFinanceTypeRequest $request, FinanceType $financeType)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(FinanceType $financeType)
    {
        //
    }
}
